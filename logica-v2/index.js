const BOARD = document.querySelector('#board');
const BTN_RESTART = document.querySelector('#restart');
const VICTORIES = document.querySelector('#victories');
const DEFEATS = document.querySelector('#defeats');
const MODAL = document.querySelector('#modal');
const BOARD_CONFIG = {
  height: 13,
  width: 30,
  difficulty: 3,
};
const TD_BASIC_CLASSES = 'py-3 rounded border-0 cursor-pointer text-light font-weight-bold text-center';
const MODAL_BASIC_CLASSES = 'w-50 p-4 m-0 rounded border-0 text-white text-center';
const TABLE_BASIC_CLASSES = 'table bg-black-transparent p-3 rounded border-0';
let mines = [];
let cellsChecked = [];
let cellsFlaged = [];
let gameFinished = false;
let missingMines = BOARD_CONFIG.difficulty;
let countVictories = 0;
let countDefeats = 0;

/** MAIN */
main();

/** Funcion inicial del programa. */
function main() {
  document.querySelector('#missingMines').innerText = missingMines;
  BTN_RESTART.style.display = 'none';
  MODAL.style.display = 'none';
  initMines();
  createBoardGrid(BOARD);
}

/** Funcion que recibe un tablero y le agrega una tabla con sus
 * filas/columnas creadas a base de la config del tablero.
 */
function createBoardGrid() {
  BOARD.innerHTML = '';
  const table = document.createElement('table');
  table.className = TABLE_BASIC_CLASSES;
  for(let y=0; y<BOARD_CONFIG.height; y++) {
    const row = document.createElement('tr');
    for(let x=0; x<BOARD_CONFIG.width; x++) {
      const column = document.createElement('td');
      column.className = TD_BASIC_CLASSES + ' bg-info';
      column.innerHTML = '&nbsp;&nbsp;';
      column.id = `cell-${y}-${x}`;
      column.onmousedown = (event) => { onMouseDown(event, y, x); };
      column.oncontextmenu = () => { return false; }
      row.appendChild(column);
    }
    table.appendChild(row);
  }
  BOARD.appendChild(table);
}

/** Funcion que crea las minas con sus respectivas posiciones. */
function initMines() {
  mines = [];
  for(let i=0; i<BOARD_CONFIG.difficulty; i++) {
    let randomX = Math.floor(Math.random() * (BOARD_CONFIG.width - 0) + 0);
    let randomY = Math.floor(Math.random() * (BOARD_CONFIG.height - 0) + 0);
    while(isMine(randomY, randomX)) {
      randomX = Math.floor(Math.random() * (BOARD_CONFIG.width - 0) + 0);
      randomY = Math.floor(Math.random() * (BOARD_CONFIG.height - 0) + 0);
    }
    mines.push(`${randomY}-${randomX}`);
  }
}

/** Funcion que valida la acción del mouse y redirige a la funcion
 * correcta.
 */
function onMouseDown(event, y, x) {
  if (gameFinished) { return; }
  switch(event.button) {
    case 0: checkCell(y,x); break;
    case 2: flagCell(y,x); break;
  }
  if (isBoardComplete() && !gameFinished) {
    finalize(true);
  }
}

/** Funcion que se ejecuta al presionar sobre la celda. Esta se
 * encarga de checkear si la celda es una mina o no.
 */
function checkCell(y, x) {
  const cell = document.querySelector(`#cell-${y}-${x}`);
  if (!isValidCell(y,x)) { return; }
  if (isFlagedCell(y,x)) { return; }
  if (isCheckedCell(y,x)) { return; }
  cellsChecked.push(`${y}-${x}`);
  if (isMine(y,x)) { finalize(false); return; }
  const minesNear = countMinesNear(y,x);
  cell.innerText = minesNear;
  cell.className = TD_BASIC_CLASSES + ' bg-secondary';
  if (minesNear === 0) {
    if (!isMine(y-1, x-1) && !isCheckedCell(y-1, x-1)) { checkCell(y-1, x-1); }
    if (!isMine(y-1, x) && !isCheckedCell(y-1, x)) { checkCell(y-1, x);   }
    if (!isMine(y-1, x+1) && !isCheckedCell(y-1, x+1)) { checkCell(y-1, x+1); }
    if (!isMine(y, x-1) && !isCheckedCell(y, x-1)) { checkCell(y, x-1);   }
    if (!isMine(y, x+1) && !isCheckedCell(y, x+1)) { checkCell(y, x+1);   }
    if (!isMine(y+1, x-1) && !isCheckedCell(y+1, x-1)) { checkCell(y+1, x-1); }
    if (!isMine(y+1, x) && !isCheckedCell(y+1, x)) { checkCell(y+1, x);   }
    if (!isMine(y+1, x+1) && !isCheckedCell(y+1, x+1)) { checkCell(y+1, x+1); }
  }
}

/** Funcion que se ejecuta al presionar click secundario sobre la celda. Esta
 * se encarga de añadir la clase flag a la celda y mostrarla como chequeada.
 */
function flagCell(y, x) {
  const cell = document.querySelector(`#cell-${y}-${x}`);
  if (isCheckedCell(y,x)) {
    return;
  }
  if (isFlagedCell(y,x)) {
    cell.className = TD_BASIC_CLASSES + ' bg-info';
    cellsFlaged.splice(cellsFlaged.indexOf(`${y}-${x}`), 1);
    missingMines++;
  } else {
    cell.className = TD_BASIC_CLASSES + ' bg-warning flag-icon';
    cellsFlaged.push(`${y}-${x}`);
    missingMines--;
  }
  document.querySelector('#missingMines').innerText = missingMines;
}

/** Funcion que recorre las celdas que rodean a la posicion
 * pasada por parametro y retorna el total de las minas
 * encontradas.
 */
function countMinesNear(y, x) {
  let count = 0;
  if (isMine(y-1, x-1)) { count++ };
  if (isMine(y-1, x)  ) { count++ };
  if (isMine(y-1, x+1)) { count++ };
  if (isMine(y, x-1)  ) { count++ };
  if (isMine(y, x+1)  ) { count++ };
  if (isMine(y+1, x-1)) { count++ };
  if (isMine(y+1, x)  ) { count++ };
  if (isMine(y+1, x+1)) { count++ };
  return count;
}

/** Funcion que indica si en la posicion pasada se encuentra
 * una mina.
 */
function isMine(y, x) {
  return mines.indexOf(`${y}-${x}`) !== -1;
}

/** Funcion que indica si la posicion pasada es una celda valida. */
function isValidCell(y, x) {
  return (y >= 0 && y < BOARD_CONFIG.height) && (x >= 0 && x < BOARD_CONFIG.width);
}

/** Funcion que indica si la posicion pasada ya fue validada. */
function isCheckedCell(y, x) {
  return cellsChecked.indexOf(`${y}-${x}`) !== -1;
}

/** Funcion que indica si la posicion pasada esta en modo flag. */
function isFlagedCell(y, x) {
  return cellsFlaged.indexOf(`${y}-${x}`) !== -1;
}

/** Funcion que verifica que el tablero esta completado. */
function isBoardComplete() {
  for(let y=0; y<BOARD_CONFIG.height; y++) {
    for(let x=0; x<BOARD_CONFIG.width; x++) {
      if (!isCheckedCell(y,x) && !isFlagedCell(y,x)) {
        return false;
      }
      if (isFlagedCell(y,x) && !isMine(y,x)) {
        return false;
      }
    }
  }
  return true;
}

/** Funcion encargada de cambiar la dificultad, es decir, la
 * cantidad de minas que se agregaran al tablero.
 */
function changeDifficulty(difficulty) {
  BOARD_CONFIG.difficulty = difficulty * 10;
  restart();
}

/** Funcion que reinicia el juego. */
function restart() {
  missingMines = BOARD_CONFIG.difficulty;
  document.querySelector('#missingMines').innerText = missingMines;
  gameFinished = false;
  BTN_RESTART.style.display = 'none';
  MODAL.style.display = 'none';
  cellsChecked = [];
  cellsFlaged = [];
  createBoardGrid();
  initMines();
}

/** Funcion que se ejecuta al finalizar un juego. Se mostrará la
 * info dependiendo del flag 'won'.
 */
function finalize(won) {
  gameFinished = true;
  if (!won) {
    mines.forEach(m => {
      const cell = document.querySelector(`#cell-${m}`);
      cell.className = TD_BASIC_CLASSES + ' bg-danger mine-icon';
    });
    MODAL.innerHTML = 'OH NO, ¡PERDISTE! 😢';
    MODAL.className = MODAL_BASIC_CLASSES + ' bg-danger';
    MODAL.style.display = 'block';
    countDefeats++;
    DEFEATS.innerText = countDefeats;
  } else {
    MODAL.innerHTML = 'MUY BIEN, ¡GANASTE! 😁';
    MODAL.className = MODAL_BASIC_CLASSES + ' bg-success';
    MODAL.style.display = 'block';
    countVictories++;
    VICTORIES.innerText = countVictories;
  }
  BTN_RESTART.style.display = 'block';
}
